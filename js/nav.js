'use strict';

    /*  This object literal takes care of opening and closing the navigation
        menu and submenus
    */
    var nav = {

        // Store opened menu/submenus
        openedMenu: [],

        // Show or hide menu/submenu usind their id
        show_hide: function(str){
            let value = this.displayValue(str);
            // Open the menu/submenu that the user is clicking on
            if(value === 'none'){
                let x = document.getElementById(str).style.display = "block";
                let t = document.getElementById("t3-sub3").style.color = "orange";
                let y = document.getElementById(str).style.color = "purple";
                let z = this.openedMenu.push(str);          
            } else {      
                // Close all menus and submenus by changing their css display to none          
                if(str === 'nav1'){
                    for(var i = 0; i < this.openedMenu.length; i++){
                        let x = document.getElementById(this.openedMenu[i]).style.display = "none";
                    };
                    let x = document.getElementById(str).style.display = "none";
                // Close only the menu where the user is clicking on
                } else {
                    let x = document.getElementById(str).style.display = "none";
                }
                
            }            
        },

        // Get the property value of the css id
        displayValue: function(str, str2){
            let element = document.getElementById(str);
            let style = window.getComputedStyle(element);
            let value = style.getPropertyValue('display');
            return value;
        }
    };
